﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace WordpressAutomation
{
    public class NewPostPage
    {


        public static void Goto()
        {
            var usersLink = Driver.Instance.FindElement(By.PartialLinkText("User"));
            usersLink.Click();
            Driver.Wait(TimeSpan.FromSeconds(1));
            var menuPosts = Driver.Instance.FindElement(By.LinkText("meaghanlewis"));
            menuPosts.Click();

            Driver.Wait(TimeSpan.FromSeconds(1));
            var addToArticle = Driver.Instance.FindElements(By.XPath("/html/body/div[2]/div/div/div/div[1]/a"));
            addToArticle[0].Click();

            Driver.Wait(TimeSpan.FromSeconds(1));
            var editToArticle = Driver.Instance.FindElement(By.PartialLinkText("Edit this article"));
            editToArticle.Click();
            Driver.Wait(TimeSpan.FromSeconds(1));
        }

        public static string Title
        {
            get
            {
                var title = Driver.Instance.FindElement(By.XPath("/html/body/div/form/div/div[1]/strong/label"));
                if (title != null)
                    return title.Text;
                return String.Empty;
            }
        }
        public static bool IsInEditMode()
        {
            return Driver.Instance.FindElement(By.Id("first-name")) != null;
        }

        public static void GoToNewPost()
        {
           var viewAllPost= Driver.Instance.FindElement(By.PartialLinkText("View all articles"));
            viewAllPost.Click();
            Thread.Sleep(2000);

        }

        public static CreatePostCommand CreatePost(string title)
        {
            return new CreatePostCommand(title);
        }

        public class CreatePostCommand
        {
            private readonly string title;
            private string body;

            public CreatePostCommand(string title)
            {
                this.title = title;
            }

           

            public CreatePostCommand WithBody(string body)
            {
                this.body = body;
                return this; 
            }

            public void Publish()
            {
                Driver.Instance.FindElement(By.Id("article_title")).Clear();
                Driver.Instance.FindElement(By.Id("article_title")).SendKeys(title);
                Driver.Instance.FindElement(By.Id("article_description")).Clear();
                Driver.Instance.FindElement(By.Id("article_description")).SendKeys(body);

                Driver.Wait(TimeSpan.FromSeconds(1));
                Driver.Instance.FindElement(By.Name("commit")).Click();
                Thread.Sleep(2000);



            }
        }
    }
}
