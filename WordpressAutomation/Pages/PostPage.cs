﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
namespace WordpressAutomation
{
    public class PostPage
    {
        public static string Title
        {
            get
            {
                var title = Driver.Instance.FindElements(By.XPath("/html/body/div[2]/div/div/div/div[1]/a"));
                if (title[0] != null)
                    return title[0].Text;
                return String.Empty;
            }
        }

    }
}
