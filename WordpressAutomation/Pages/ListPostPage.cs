﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace WordpressAutomation
{
    public class ListPostPage
    {
        private static int lastCount;

        public static int PreviousPostCount
        {
            get
            {
                return lastCount;
            }
        }

        public static int CurrentPostCount
        {
            get
            {
                return GetPostCount();
            }
        }

        public static void GoTo(PostType postType)
        {
            switch (postType)
            {

                case PostType.Page:
                    Navigation.Pages.All.Select();
                    break;

                case PostType.Posts:
                    Navigation.Pages.All.Select();
                    break;
            }
        }

        public static void TrashPost(string title)
        {
            var buttons = Driver.Instance.FindElements(By.XPath("/html/body/div[2]/div[1]/div/div/div[3]/a"));
            foreach (var item in buttons)
            {
                ReadOnlyCollection<IWebElement> links = null;
                Driver.NoWait(() => item.FindElements(By.LinkText(title)));
                if (links.Count > 0)
                {
                    Actions action = new Actions(Driver.Instance);
                    action.MoveToElement(links[0]);
                    action.Perform();
                    item.FindElement(By.LinkText("Delete article")).Click();
                    return;

                }

            }

        }

        public static bool DoesPostExitsWithTitle(string title)
        {
            return Driver.Instance.FindElements(By.LinkText(title)).Any();
        }

        public static void StoreCount()
        {
            lastCount = GetPostCount();
        }

        private static int GetPostCount()
        {
            var countText = Driver.Instance.FindElements(By.XPath("/html/body/div/form/div/div"));
            return countText.Count;
        }
        public static void SelectPost(string title)
        {
            var firstNameField = Driver.Instance.FindElement(By.Id("first-name"));
            firstNameField.Clear();
            Thread.Sleep(1000);
            firstNameField.SendKeys(title);


        }


        public enum PostType
        {
            Page,
            Posts
        }

    }
}

