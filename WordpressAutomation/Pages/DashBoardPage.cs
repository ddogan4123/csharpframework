﻿using System;

using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace WordpressAutomation
{
    public class DashBoardPage
    {
        public static bool IsAt
        {
            get { 
            var alert = Driver.Instance.FindElements(By.ClassName("alert-success"));
                if (alert.Count > 0)
                    return alert[0].Text.Contains("successfully");
                return false;
                
            }
        }
    }
}
