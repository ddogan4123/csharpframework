﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace WordpressAutomation
{
    public class LoginPage
    {
        public static void GoTo()
        {

            Driver.Instance.Navigate().GoToUrl("http://selenium-blog.herokuapp.com/login");
            var wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(10));
            wait.Until(d => d.SwitchTo().ActiveElement().GetAttribute("id") == "session_email");
           
           
        }

        public static void GoToFormy()
        {

            Driver.Instance.Navigate().GoToUrl("http://formy-project.herokuapp.com/");


        }

        public static LoginCommand LoginAs(string userName)
        {
            return new LoginCommand(userName);
        }

        public class LoginCommand
        {
            private readonly string userName;
            private string password;

            public LoginCommand(string userName)
            {
                this.userName = userName;
            }



            public LoginCommand WithPassword(string password)
            {
                this.password = password;
                return this; 
            }

            public void Login()
            {
                var loginInput = Driver.Instance.FindElement(By.Id("session_email"));
                loginInput.SendKeys(userName);

                var passwordInput = Driver.Instance.FindElement(By.Id("session_password"));
                passwordInput.SendKeys(password);

                var loginButton = Driver.Instance.FindElement(By.Name("commit"));
                loginButton.Click();
            }
        }

       
    }

   
}
