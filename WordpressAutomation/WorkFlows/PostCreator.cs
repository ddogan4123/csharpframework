﻿using System;
namespace WordpressAutomation
{
    public class PostCreator
    {
        public static string PreviousTitle { get; set; }
        public static string PreviousBody { get;  set; }

        public static void CreatePost()
        {
            NewPostPage.Goto();
            PreviousTitle = CreateTitle();
            PreviousBody = CreateBody();

            NewPostPage.CreatePost(PreviousTitle)
                .WithBody(PreviousBody)
                .Publish();


        }

        private static string CreateTitle()
        {
            throw new NotImplementedException();
        }

        private static string CreateBody()
        {
            throw new NotImplementedException();
        }
    }
}
