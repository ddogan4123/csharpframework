﻿using System;
using NUnit.Framework;
using WordpressAutomation;

namespace WordpressTests
{
    public class WordpressTest
    {
        [SetUp]
        public void Init()
        {
            Driver.Initialize();
        }

        [TearDown]
        public void CleanUp()
        {
            Driver.Close();
        }
    }
}
