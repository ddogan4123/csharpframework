﻿using System;
using NUnit.Framework;
using WordpressAutomation;
using static WordpressAutomation.ListPostPage;

namespace WordpressTests.PostTest
{
    public class AllPostsTests : WordpressTest
    {
       [Test]
       public void Added_Post_Show_Up()
        {
            LoginPage.GoToFormy();
            ListPostPage.GoTo(PostType.Posts);
            ListPostPage.StoreCount();


            LoginPage.GoTo();


            LoginPage.LoginAs("admin_4123@gmail.com").WithPassword("password").Login();
            NewPostPage.Goto();
            NewPostPage.CreatePost("This is the test post title")
                .WithBody("Hi,this is the body")
                .Publish();
            NewPostPage.GoToNewPost();

            LoginPage.GoToFormy();
            ListPostPage.GoTo(PostType.Posts);
            Assert.AreEqual(ListPostPage.PreviousPostCount, ListPostPage.CurrentPostCount, "They did not matched");

            Assert.IsFalse(ListPostPage.DoesPostExitsWithTitle("Added posts show up, title"));

            LoginPage.GoTo();


            LoginPage.LoginAs("admin_4123@gmail.com").WithPassword("password").Login();
            NewPostPage.Goto();
            NewPostPage.CreatePost("This is the test post title")
                .WithBody("Hi,this is the body")
                .Publish();
            ListPostPage.TrashPost("Added posts show up title");
            Assert.AreNotEqual(ListPostPage.PreviousPostCount, ListPostPage.CurrentPostCount, "Could not trash");
            	
        }
    }
}
