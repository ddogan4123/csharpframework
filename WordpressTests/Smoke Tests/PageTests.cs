﻿using System;
using NUnit.Framework;
using WordpressAutomation;

namespace WordpressTests
{
    public class PageTests : WordpressTest

    {
       

        [Test]
        public void Can_Edit_First_Name_Field()
        {
            LoginPage.GoToFormy();

            ListPostPage.GoTo(ListPostPage.PostType.Page);
            ListPostPage.SelectPost("first-name");

            Assert.IsTrue(NewPostPage.IsInEditMode(), "Wasn't in edit mode");
            Assert.AreEqual("First name", NewPostPage.Title, "Title did not match");

        }


       
    }
}
