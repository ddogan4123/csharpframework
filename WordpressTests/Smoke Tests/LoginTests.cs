using NUnit.Framework;
using WordpressAutomation;
using WordpressTests;

namespace Tests
{
    
    public class LoginTests : WordpressTest
    {
      


        [Test]
        public void Admin_User_Can_Login()
        {
            LoginPage.GoTo();
            LoginPage.LoginAs("admin_4123@gmail.com").WithPassword("password").Login();

           
            Assert.IsTrue(DashBoardPage.IsAt, "Failed to login");
        }


      
    }
}