﻿using System;
using NUnit.Framework;
using WordpressAutomation;

namespace WordpressTests
{
    public class CreatePostTests : WordpressTest
    {
       
        [Test]
        public void Can_Create_A_Basic_Post()
        {
            LoginPage.GoTo();


            LoginPage.LoginAs("admin_4123@gmail.com").WithPassword("password").Login();
            NewPostPage.Goto();
            NewPostPage.CreatePost("This is the test post title")
                .WithBody("Hi,this is the body")
                .Publish();

            NewPostPage.GoToNewPost();
            Assert.AreEqual(PostPage.Title, "This is the test post title", "Title did not match new post");
        }


     
    }
}
